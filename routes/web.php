<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Route::get('/', function () {
    return view('welcome');
}); */
	//Route::get('/','HomeController@login');
	Route::get('/','HomeController@login');
	//User activate
	Route::get('/account/signup/{token}','ActivateController@activate_user');
	Route::get('/activate/new_user/{token}','ActivateController@user_new');

Route::group(['middleware' => 'auth'], function () {
   //User management 
	Route::get('user/manage','UserController@searchUser')->middleware('admin');
	Route::post('user/manage','UserController@postSearchUser')->middleware('admin');
	Route::get('user/edit/{id}','UserController@getEditUser');
	Route::post('user/edit','UserController@postEditUser');
	//File Upload
	Route::get('/upload', 'HomeController@s3upload')->middleware('admin');
	Route::get('/upload_success', 'HomeController@s3uploadsucess')->middleware('admin');
	Route::post('/user/update', 'UserController@edit_user');
	Route::get('/user/courses', 'UserController@user_courses');
	//Course management
	Route::get('/courses/new','CourseController@new_course')->middleware('admin');
	//Route::post('/courses/new_section','CourseController@new_post_course');
	//Route::post('/courses/preview','CourseController@new_post_section');
	Route::get('/courses/new_section','CourseController@new_post_section')->middleware('admin');
	Route::get('/courses/section_add','CourseController@new_section_add')->middleware('admin');
	Route::post('/courses/save','CourseController@new_post_preview')->middleware('admin');
	Route::get('/course/view/{id}','CourseController@view_course');
	Route::get('course/manage','CourseController@searchCourse')->middleware('admin');
	Route::post('course/manage','CourseController@postSearchCourse')->middleware('admin');
});

