@extends('adminlte::layouts.auth')

@section('content')
<body class="hold-transition login-page">
    <div id="app" v-cloak>
        <div class="login-box">
            <div class="login-logo">
                <a href="{{ url('/home') }}"><b>PASSWORD EDIT</b></a>
            </div><!-- /.login-logo -->

                <div class="login-box-body">

				<!-- <form  method="POST" action="{{url('/activate/update/'.$id)}}" accept-charset="UTF-8" id="adduser">
                          <div class="form-group">
                            <label for="name">User Name:</label>
                            <input type="text" class="form-control" id="name" name="name">
                          </div>
                          <div class="form-group">
                            <label for="password">Password:</label>
                            <input type="password" class="form-control" id="password" name="password">
                          </div>
                          <div class="checkbox">
                          </div>
                          <button type="submit" class="btn btn-default">Submit</button>
                    </form> -->

                     {!! Form::open(['url'=>'activate/update/'.$id,  'class' => 'form-horizontal', 'role' => 'form', 'files' => 'true', 'method' =>'POST','enctype'=>'multipart/form-data','files'=>true ]) !!}
                     <div class="form-group">
                            <label for="name">User Name:</label>
                            <input type="text" class="form-control" id="name" name="name" placeholder="Enter Your Username">
                          </div>
                          <div class="form-group">
                            <label for="password">Password:</label>
                            <input type="password" class="form-control" id="password" name="password" placeholder="Enter Your Password">
                          </div>
                          <div class="form-group" style="width: 100%;    margin-left: 0px; ">
                                   <input type="submit" value="Submit" class="pull-right btn btn-primary">
                            </div>
                     {!! Form::close() !!}

                </div>
        </div>
    </div>
    @include('adminlte::layouts.partials.scripts_auth')
</body>

@endsection
