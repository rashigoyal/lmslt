 @extends('adminlte::layouts.auth')
 @section('content')
                 @if(Session::has('user_updated'))
            <div class="alert alert-success"><span class="glyphicon"></span><em> {!! session('user_updated') !!}</em></div>
            @endif  
<body class="hold-transition login-page">
    <div id="app" v-cloak>
        <div class="login-box">
            <div class="login-logo">
				<b>Create your account</b>
				<h4> Please complete your account details </h4>
			</div><!-- /.login-logo -->


                <div class="login-box-body" style="padding:40px;">
	                        {!! Form::open(['url'=>'/account/signup', 'class' => 'form-horizontal', 'role' => 'form', 'files' => 'true','method' =>'POST' ]) !!}
									<input type="hidden"  value="<?php echo $user_id ?>" name="user_id" class ='form-control' >
									<input type="hidden"  value="<?php echo $data_id ?>" name="token_salt" class ='form-control' >
								<div class="form-group has-feedback">
									<input type="text" readonly="readonly" value="<?php echo $email ?>" name="email" placeholder = 'Email' id='email' class ='form-control' >
									<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
								</div>
								<div class="form-group has-feedback">
									{!! Form::text('user_con', null, ['id' => 'user_con', 'class' => 'form-control','required'=>'required', 'placeholder' => 'Enter User Name']) !!}

									<span class="glyphicon glyphicon-user form-control-feedback"></span>
                                </div>
								<div class="form-group has-feedback">
									<input type="password"  placeholder="Enter Password" name="pass_con" id="pass_con" class='form-control' >
									<span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                </div>								
                                <div class="form-group has-feedback">   
									<button  style="width: 100%;" type="submit" id="new_pass_submit" class="btn btn-primary btn-block btn-flat">Continue</button>
								</div>
								{!! Form::close() !!}
			   </div>
        </div>
    </div>
    @include('adminlte::layouts.partials.scripts_auth')
</body>

@endsection
			