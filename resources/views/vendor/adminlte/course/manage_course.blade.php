@extends('adminlte::layouts.app')

@section('main-content')
<div class="wrapper"> 
    <div class="content-wrapper">
        <div class="container">
            <div class="content">
			
			<div class="col-md-11 basic user-management">
					<div id="tab1_div" class="box box-primary">
						<div class="box-header with-border" style="background: linear-gradient(#0275af , #135886 );">
              <h3 class="box-title" style="color: white;">Manage Course</h3>
            </div>

						<div class="box-header with-border" id="filter_user">
						<div class="error" id='err_div' style='display:none'></div> 
						{!! Form::open(['url'=>'course/manage','id' => 'searchUser', 'class' => 'form-horizontal', 'role' => 'form', 'files' => 'true', 'method' =>'POST' ]) !!}
							
			
								
								
								
								
								<div class="form-group">
                                    <label for="name" class="col-lg-3 control-label"> Course Title</label>
                                    <div class="col-lg-9">
                                    	{!! Form::text('course_name', null, ['id' => 'course_name', 'class' => 'form-control', 'placeholder' => 'Enter course title']) !!}
                                    </div>
                                </div>
								
								
								
								<div class="form-group col-md-12 col-sm-12 col-xs-12">
									<button type="submit" id="save" class="pull-right btn btn-primary">Search</button>
								</div>
							{!! Form::close() !!}
						</div>
					</div>
	            </div>
			
			
			
			
			
			
			
			
			
			@if(!empty($course))
					@if(!empty($course->items()))
                <div class="col-md-11 basic user-management">
                  					<div class="box-header with-border" style="background: rgba(0, 0, 0, 0) linear-gradient(rgb(2, 117, 175), rgb(19, 88, 134)) repeat scroll 0% 0%;">
					<h3 class="box-title" style="color: white;"><i class="fa fa-cogs"></i> Search Results  </h3></div>

				            <div class="box" style="border-top:none;">

            <!-- /.box-header -->
            <div class="box-body">
			
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  
                  <th>Course Name</th>
                  <th>Section</th>
				  <th>Users</th>
                  <th>Price</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
				  <?php $i = 0; ?>
                      @foreach ($course as $key) 
					<tr>
                 
                  <td><a href="{{url('course/view/')}}/{{$key->id}}">{{$key->title}}</a></td>
                  <td><?php echo count($key->sections);?></td>
				  <td><?php echo count($key->users);?></td>
                  <td>{{$key->price}}</td>
                  <td><a href="{{url('course/edit/')}}/{{$key->id}}">Edit</a> </td>
                </tr>
				
				
					     <?php $i++; ?>
                      @endforeach
				
                </tbody>
              </table>
			  
            </div>
            <!-- /.box-body -->
			{{ $course->render() }}
          </div>
				
		
		  
				  
                 </div>@endif
				@endif
            </div>
        </div>
     </div>
 </div>
@endsection