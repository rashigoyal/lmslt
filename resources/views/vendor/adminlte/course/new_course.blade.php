
<?php $my_bucket ='digistaklms';?>
@extends('adminlte::layouts.app')



@section('main-content')
<div class="wrapper"> 

  

    <div class="content-wrapper">
        <div class="container">
            

            <div class="content">

                <div class="col-md-11 basic user-management">
                 
                    <div id="tab1_div" class="box box-primary">
                 
                     
            <div class="box-header with-border" style="background: linear-gradient(#0275af , #135886 );">
              <h3 class="box-title" style="color: white;">New Course</h3>
            </div>


  						<div class="box-header with-border">


                            <div class="error" id='err_div' style='display:none'></div> 

                            	 {!! Form::open(['url'=>'courses/new_section','id' => 'importcsv', 'class' => 'form-horizontal', 'role' => 'form', 'files' => 'true', 'method' =>'POST','enctype'=>'multipart/form-data','files'=>true ]) !!}


								<div class="form-group" id='clientName' style='display:block'>
	                                <label for="name" class="col-lg-3 control-label required">Title</label>
	                                <div class="col-lg-9">
		                             
		                                   {!! Form::text('title', null, ['id' => 'course_title', 'class' => 'form-control course_title','required'=>'required', 'placeholder' => 'Enter course title']) !!}
									
	                                </div>
	                            </div>

								<div class="form-group" id='clientName' style='display:block'>
	                                <label for="name" class="col-lg-3 control-label required">Price(&#x24;)</label>
	                                <div class="col-lg-9">
		                             
		                                   {!! Form::text('price', null, ['id' => 'course_title', 'class' => 'form-control course_title','required'=>'required', 'placeholder' => 'Enter price in dollars']) !!}
									
	                                </div>
	                            </div>



                        <div class="form-group date-section">

                              <label for="call_date" class="col-lg-3 control-label required"> Duration</label>

                                <div class="col-lg-5 col-xs-6 padding-right">

                                    <input type="number" class="form-control" name="duration_value" value="10" min="1" step="1">

                                  </div>

                                <div class="col-lg-4 col-xs-6 padding-right">

                                       <select name="duration_weeks" class="form-control">
                                               <option value="minute">Minute(s)</option>
                                                  <option value="hours">Hour(s)</option>
                                                   <option value="days">Day(s)</option>
                                                    <option selected="" value="weeks">Week(s)</option>

                                       </select>

                                </div>



                        </div>

                       <div class="form-group" id='clientName' style='display:block'>
                         <label for="call_date" class="col-lg-3 control-label required"> Schedule</label>
                           <div class="col-lg-9">
                           <select name="schedule" class="form-control">
                            <option value="mon-fri">Monday to Friday</option>
                            <option value="saab">Weekends (Saturday and Sunday)</option>
                          </select>
                            </div>

                       </div>



							<div class="form-group" id='clientName' style='display:block'>
                                <label for="name" class="col-lg-3 control-label required">Description</label>
                                <div class="col-lg-9">
	                             
									{!! Form::textarea('description', null, ['id' => 'course_desc', 'class' => 'form-control course_desc', 'rows' => 5, 'cols' => 40, 'placeholder' => 'Enter  description']) !!}
								
                                </div>
                            </div>


							<div class="form-group" id='clientName' style='display:block'>
                                <label for="name" class="col-lg-3 control-label required">Summary</label>
                                <div class="col-lg-9">
	                             
									{!! Form::textarea('summary', null, ['id' => 'course_sum', 'class' => 'form-control course_sum', 'rows' => 2, 'cols' => 40, 'placeholder' => 'Enter  summary']) !!}
								
                                </div>
                            </div>

                          <div class="form-group" id='clientName' style='display:block'>
                              <label for="name" class="col-lg-3 control-label required">Skills</label>
                                <div class="col-lg-9">
                               
                  {!! Form::textarea('skills', null, ['id' => 'skills', 'class' => 'form-control skills', 'rows' => 2, 'cols' => 40, 'placeholder' => 'Enter  comma separated skills']) !!}
                
                                </div>
                            </div>


                            <div class="form-group" id='clientName' style='display:block'>
                                <label for="name" class="col-lg-3 control-label required">Mode</label>
                                <div class="col-lg-9">
                                 <select name ="mode" id="direct_q" class="form-control">
                                     <option selected value="1">Online </option>
                                     <option  value="2">Instructor led</option>
                                 </select>
                                </div>
                            </div>








                        <div class="form-group" style="width: 100%;    margin-left: 0px; ">
                             
							                     <input type="submit" value="Save" class="pull-right btn btn-primary">
                            {!! Form::close() !!}
                           




                       </div>


                        <div class="form-group col-md-11 col-sm-11 col-xs-11"><span class="mandatory">Fields marked with an asterisk (*) are mandatory</span>
                            </div>


                     </div>

                 </div>


            </div>

         </div>
     </div>

 </div>


                                










@endsection