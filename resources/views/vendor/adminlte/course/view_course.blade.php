@extends('adminlte::layouts.app')
@section('main-content')
<div class="wrapper"> 
    <div class="content-wrapper">
        <div class="container">
		<section class="invoice col-md-11">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="fa fa-globe"></i> {{$course->title}}
            <small class="pull-right">Date: {{$course->created_at}}</small>
          </h2>
        </div>
        <!-- /.col -->
      </div>
	  <div class="row invoice-info">
        <div class="col-sm-12 invoice-col">
		{{$course->description}}
		</div>
		</div>
		 <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
		
		</div>
		</div>
      <!-- info row -->
      <div class="row invoice-info">
          <!--<div class="col-sm-4 invoice-col">
          Price
        <address>
            <strong>Admin, Inc.</strong><br>
            795 Folsom Ave, Suite 600<br>
            San Francisco, CA 94107<br>
            Phone: (804) 123-5432<br>
            Email: info@almasaeedstudio.com
          </address>
        </div>
       
        <div class="col-sm-4 invoice-col">
          To
          <address>
            <strong>John Doe</strong><br>
            795 Folsom Ave, Suite 600<br>
            San Francisco, CA 94107<br>
            Phone: (555) 539-1037<br>
            Email: john.doe@example.com
          </address>
        </div>-->
        <!-- /.col -->
		<br>
        <div class="col-sm-4 invoice-col">
          <b>Price: </b>{{$course->price}} <br>
          <b>Schedule:</b> {{$course->schedule}}<br>
          <b>Duration:</b> {{$course->duration}}<br>
          <b>Skills:</b> {{$course->skills}}<br>
        </div>
		<br>
        <!-- /.col -->
      </div><br>
      <!-- /.row -->

		<div class="row">
    <div class="col-md-12">
        <!-- Course Widget -->
                <div class="widget">
            <div class="widget-advanced">
                <!-- Widget Header -->
          
                <!-- END Widget Header -->

                <!-- Widget Main -->
                <div class="widget-main">
				<?php foreach($course['sections'] as $sec){?>
                    <!-- Lessons -->
                <table class="table table-vcenter">
                        <thead>
                            <tr class="active">
                                <th><i class="gi gi-package"></i> {{$sec->section_name}}</th>
                                <th></th>
                                <!-- <th class="text-right"><small><em>1 hour</em></small></th> -->
                            </tr>
                        </thead>
                        <tbody>
                                                        <tr>
                                
                                
									<!-- <a class="btn btn-xs btn-info" data-toggle="tooltip" title="" href="#" data-original-title="Viewed"><i class="fa fa-check"></i></a>-->
									
									<?php 
									 foreach($video as $vid){
										 if($vid->id == $sec->video_id ){
									?>
									<td><a href=""><i class="fa fa-ellipsis-h"></i> {{$vid->key}}</a></td>
									<td class="text-right">
<a class="btn btn-xs btn-success" href="<?php echo 'http://'.$vid->bucket.'.s3.amazonaws.com/'.$vid->key;?>">
									<i class="fa fa-youtube-play" data-toggle="tooltip" title="" data-original-title="Is Video"></i>
									</a>  </td>									
									 <?php }}?>
                              
                                <!-- <td class="text-right"><a href="#" class="btn btn-xs btn-success" data-toggle="tooltip" title="Done!"><i class="fa fa-check"></i></a></td>-->
                            </tr>
                                                        
                                                    </tbody>
                    </table>
                <?php }?>                        
                                        <!-- END Lessons -->
                </div>
                <!-- END Widget Main -->
            </div>
        </div>
        <!-- END Course Widget -->
    </div>
    
</div>


      <!-- /.row -->

      <!-- this row will not appear when printing -->
      
    </section>
          
                        <!-- Courses Header -->


<!-- END Courses Header -->

<!-- Main Row -->

<!-- END Main Row -->

                    </div>  
     </div>

 </div>

@endsection



