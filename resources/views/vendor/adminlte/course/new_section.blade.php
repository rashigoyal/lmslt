
@extends('adminlte::layouts.app')




@section('main-content')

<div class="wrapper"> 

  

    <div class="content-wrapper">
        <div class="container">
            

            <div class="content">

                <div class="col-md-11 basic user-management">
                 
                    <div id="tab1_div" class="box box-primary">
                 
                     
            <div class="box-header with-border" style="background: linear-gradient(#0275af , #135886 );">
              <h3 class="box-title" style="color: white;">Add Section</h3>
            </div>


  						<div class="box-header with-border">


                            <div class="error" id='err_div' style='display:none'></div> 

                            	 {!! Form::open(['url'=>'courses/save','id' => 'importcsv', 'class' => 'form-horizontal', 'role' => 'form', 'files' => 'true', 'method' =>'POST','enctype'=>'multipart/form-data','files'=>true ]) !!}


                                <input type="hidden" name="course_id" id="course_id" class="course_id" value=<?php echo$course->id  ?> >  
                                                                <input type="hidden" name="counter" id="counter" class="counter" value="0">  
							   <div id="dynamicInput">
                            	<!-- <div class="form-group" id='clientName' style='display:block'>
	                                <label for="name" class="col-lg-3 control-label required">Section Title</label>
	                                <div class="col-lg-9">
		                             
		                                   {!! Form::text('title', null, ['id' => 'section_title', 'class' => 'form-control section_title','required'=>'required', 'placeholder' => 'Enter section title']) !!}
									
	                                </div>
	                            </div> -->

                                </div>
                                        <div class="form-group" style="width: 100%;    margin-left: 0px; ">
                <button id="b1" style="float: right;" class="btn btn-primary add-more" type="button" onClick="addInput('dynamicInput');">Add Section</button><br>
                </div>
<!-- 
                  <div class="form-group" id="add_vides" style="width: 100%;    margin-left: 0px; ">
               <button type='button' id='click_dash' class='btn btn btn-primary ' data-toggle='modal' data-target='#mymodal' data-backdrop='false' style='float: right;'>Add Videos</button>
                </div> -->
    




        <div class="modal fade" id="mymodal" role="dialog">
            
            <div class="modal-dialog">
                
                <!-- Modal content-->

                
                <div class="modal-content" style="width: 300px; margin-left: auto; margin-right: auto;">
                    
                    <div class="modal-header" style="background: linear-gradient(#0275af , #135886 ) !important; padding: 9px 15px 12px 15px;
                        color: #fff; font-weight: bold !important; border-bottom: 2px solid #bbbbbb;">
                        
                        <button type="button" class="close" data-dismiss="modal" style="color:#fff;opacity: 1;">×</button>
                        
                        <h4 class="modal-title">  MultiMedia   </h4>
                    
                    </div>
                    
                    <div class="modal-body" style="height: 400px; overflow-y: scroll;padding:0px;">
                        
                        <table class="table no-margin font14 api_user" style="height:400px;overflow-y:scroll;">
                            <thead>
                                
                                <tr>
                                    
                                    <td>


                                    </td>
                                
                                </tr>
                                
                            
                            </thead>
                            <tbody>
                                
                                @foreach ($s3 as $key)                              
                                
                                <tr>
                                    
                                    <td>
                                        
                                      <input type="checkbox" name="account_idbox[]" value="<?php echo $key->id?>">
                                         <input  type="hidden" name="counter1" id="counter1" class="counter1" value="0"> 
                        

                                    </td>
                        
                             
                                    
                                    <td>
                                        
                                        {{ $key-> key }}
                                    
                                    </td>
                                
                                </tr>
                                    
                                    @endforeach
                               
                            </tbody>
                        
                        </table>
                    
                    </div>
                    
                    <div class="modal-footer">
                        
                        <button type="button" id="click_dash_submit" class="btn btn-sm btn-default btn-flat btn_int_all" data-dismiss="modal">Close</button>
                    
                    </div>
                
                </div>
            
            </div>
        
        </div>



        <script type="text/javascript">
            
          


        </script>



          








							           	<div class="form-group" style="width: 100%;    margin-left: 0px;">
                             
							                     <input  type="submit" value="Save" class="pull-right btn btn-primary">
                            {!! Form::close() !!}
                           




                       </div>






                      


                     </div>

                 </div>


            </div>

         </div>
     </div>

 </div>

<script src="{{ asset('/js/add-section.js') }}"></script>
                                










@endsection



