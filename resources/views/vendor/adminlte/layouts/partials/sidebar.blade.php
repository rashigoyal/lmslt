<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        @if (! Auth::guest())
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="{{ Gravatar::get($user->email) }}" class="img-circle" alt="User Image" />
                </div>
                <div class="pull-left info">
                    <p>{{ Auth::user()->name }}</p>
                    <!-- Status -->
                    <a href="#"><i class="fa fa-circle text-success"></i> {{ trans('adminlte_lang::message.online') }}</a>
                </div>
            </div>
        @endif

        <!-- search form (Optional)
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="{{ trans('adminlte_lang::message.search') }}..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
        </form> -->
        <!-- /.search form -->
		@if(Auth::user()->role == 0)
		<ul class="sidebar-menu">
            <li class="header">User Management</li>
            <li @if ($menu['item'] == 'user-manage') class="active" @endif><a href="{{ url('user/manage') }}"><i class="fa fa-users"></i> <span>Manage User</span></a></li>
        </ul><!-- /.sidebar-menu -->
        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">File Management</li>
            <li @if ($menu['item'] == 'file-upload') class="active" @endif><a href="{{ url('upload') }}"><i class=' fa fa-cloud-upload'></i> <span>File Upload</span></a></li>
        </ul><!-- /.sidebar-menu -->
        <ul class="sidebar-menu">
            <li class="header">Course Management</li>
           <li class="treeview">
                <a href="#"><i class='fa fa-book'></i> <span>Course</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                     <li @if ($menu['item'] == 'add-course') class="active" @endif><a href="{{ url('/courses/new') }}"><i class='fa fa-link'></i>Add Course</i></a></li>
                      <li @if ($menu['item'] == 'manage-course') class="active" @endif><a href="{{ url('/course/manage') }}"><i class='fa fa-link'></i>Manage Course</i></a></li>
                </ul>
            </li>
        </ul>
		@endif
		@if(Auth::user()->role == 1)
			<ul class="sidebar-menu">
            <li class="header">User Management</li>
            <li @if ($menu['item'] == 'user-manage') class="active" @endif><a href="{{ url('user/edit/') }}/{{Auth::user()->id}}"><i class="fa fa-users"></i> <span>Edit Profile</span></a></li>
			<li @if ($menu['item'] == 'course-my') class="active" @endif><a href="{{ url('user/courses/') }}"><i class="fa fa-users"></i> <span>My Courses</span></a></li>

	  </ul>
		@endif
		<!-- /.sidebar-menu -->
		<!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li >
				<a href="{{ url('/logout') }}" id="logout" onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
					<i class="fa fa-sign-out" aria-hidden="true"></i>  {{ trans('adminlte_lang::message.signout') }}
                </a>
				<form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
					{{ csrf_field() }}
					<input type="submit" value="logout" style="display: none;">
				</form></a>
			</li>
        </ul>
		<!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
