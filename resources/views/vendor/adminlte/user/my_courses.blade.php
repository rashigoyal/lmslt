@extends('adminlte::layouts.app')

<link rel="stylesheet" href="{{ asset('css/user.css')}}">

@section('main-content')
 <div class="content-wrapper">
 <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        
        <!--  <small>Control panel</small> 
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>-->
    </section>
	<section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
				<?php if($user->role ==1){?>
		<div class="col-md-10">
          <!-- Info Boxes Style 2 -->
		  <?php // dd($userCourse);?>
		  <!-- @foreach($userCourse['courses'] as $val)
		  <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box bg-yellow">
            <span class="info-box-icon"><i class="ion ion-ios-pricetag-outline"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">{{$val->title}}</span>
              <span class="info-box-number">{{$val->price}}</span>
              <div class="progress">
                <div class="progress-bar" style="width: 100%;color:black"></div>
              </div>
                  <span class="progress-description">
                   <a href="{{ url('course/view/') }}/{{$val->id}}" style="color:#fff">View Course</a>
                  </span>
            </div>
          </div>
		  </div>
		  @endforeach -->
 <div class="container">
  <div class="row">
 @foreach($userCourse['courses'] as $val)
    <div class="col-lg-2 col-sm-6">
      <div class="circle-tile">
        <div class="circle-tile-heading bg-blue"><i class="ion ion-ios-pricetag-outline" style="font-size: 51px; margin-top: 12px;"></i></div>
        <div class="circle-tile-content bg-blue">
          <div class="circle-tile-number text-faded ">{{$val->title}}</div>
          <div class="circle-tile-number text-faded ">{{$val->price}}</div>
          <a class="circle-tile-footer bg-yellow" href="{{ url('course/view/') }}/{{$val->id}}">View Course <i class="fa fa-chevron-circle-right"></i></a>
        </div>
      </div>
    </div>
@endforeach
 </div> 
</div>
</div>
</div>
		  
          <!-- /.info-box -->
          <!-- /.box -->
        </div>
				<?php }?>
        <!-- ./col -->
      </div>
	  <div class='row'>
	  <?php if($user->role ==0){?>
	  <div class="col-md-4">
	<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Recently Added Courses</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <!--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>-->
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body" style="display: block;">
              <ul class="products-list product-list-in-box">
			  @foreach($course as $val)
                <li class="item">
                  <!--<div class="product-img">
                    <img src="dist/img/default-50x50.gif" alt="Product Image">
                  </div>-->
                  <div class="product-info">
                    <a href="javascript:void(0)" class="product-title">
                      <span class="label label-warning pull-right">{{$val->price}}</span></a>
                        <span class="product-description">
						{{$val->title}}
                        </span>
                  </div>
                </li>
				@endforeach
                <!-- /.item -->
                
              </ul>
            </div>
            <!-- /.box-body -->
            <div class="box-footer text-center" style="display: block;">
              <a href="{{url('course/manage')}}" class="uppercase">View All Courses</a>
            </div>
            <!-- /.box-footer -->
          </div></div>
		  <?php }?>
	  </div>
      <!-- /.row -->
      <!-- Main row -->
      
      <!-- /.row (main row) -->

    </section>
</div>
@endsection
