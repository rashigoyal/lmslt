@extends('adminlte::layouts.app')


@section('main-content')
<div class="wrapper">

<div class="content-wrapper">
   <div class="container">
      <div class="content">
         <div id="email_val" class="alert alert-danger" style="display:none">
            Email is already taken
         </div>
         <div id="name_val" class="alert alert-danger" style="display:none">
            Client does not exist
         </div>
         @if(Session::has('add_user_message'))
         <div class="alert alert-success text-center flash-alert" >
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            {{Session::get('add_user_message')}}
         </div>
         @endif
		  @if(Session::has('user_updated'))
         <div class="alert alert-success text-center flash-alert" >
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            {{Session::get('user_updated')}}
         </div>
         @endif
        
         <div class="col-md-11 basic user-management">
            <div id="tab1_div" class="box box-primary">
               	<div class="box-header with-border" style="background: linear-gradient(#0275af , #135886 );">
              <h3 class="box-title" style="color: white;"><i class="fa fa-pencil" aria-hidden="true"></i>  Edit User</h3>
            </div>
               <div class="box-header with-border">
                  <div class="error" id='err_div' style='display:none'></div>
                  <input type="hidden" id="roleset" name="client_id" value="<?php echo $user->role;?>">
                 
                  {{ Form::open(['url'=>'user/edit','id' => 'adduser', 'class' => 'form-horizontal', 'role' => 'form', 'files' => 'true', 'method' =>'POST' ]) }}
                 

                  
                 
		
                 
			

                 
                
                 
                  <div class="form-group">
                     <label for="name" class="col-lg-3 control-label required">User Name</label>
                     <div class="col-lg-9">
                        
                        <input type="hidden" id="action" name="" value="edit">
                        <input type="hidden" id="user_id" name="user_id" value="{{$userdata['id']}}">
                        {!! Form::text('user_name', $userdata['name'], ['required'=>'required','id' => 'user_name', 'class' => 'form-control', 'placeholder' => 'Enter user name'])!!}
                       
                     </div>
                  </div>
                  <div class="form-group">
                     <label for="name" class="col-lg-3 control-label required">User Email</label>
                     <div class="col-lg-9">
                       
                        {!! Form::email('user_email',  $userdata['email'], ['required'=>'required','id' => 'email', 'class' => 'api_inputs form-control', 'placeholder' => 'Enter user email'])!!}
                       
                     </div>
                  </div>
                  <div class="form-group">
                     <label for="name" class="col-lg-3 control-label required"> User Password</label>
                     <div class="col-lg-9">
                        <?php if($action == 'edit'){?>
                        <input id="password" class="form-control" required="required" placeholder="Enter Password" name="user_password" value="{{$userdata['password']}}" type="text">
                        <?php }else {?>
                        {!! Form::password('password', ['required'=>'required','id' => 'password', 'class' => 'form-control', 'placeholder' => 'Enter Password'])!!}
                        <?php }?>
                     </div>
                  </div>
               
                  <div class="form-group">
                     <label for="name" class="col-lg-3 control-label required"> Status</label>
                     <div class="col-lg-9">
                        {!! Form::select('status', ['1' => 'Active', '0' => 'Inactive'], $userdata->status, ['class' => 'form-control']) !!}
                     </div>
                  </div>
		
				   <div class="form-group" >
                     <label for="name" class="col-lg-3 control-label "> Created On</label>
                     <div class="col-lg-9">
<input id="" class="form-control" disabled="disabled"   value="{{$userdata->created_at}}" type="text">                     </div>
                     </div>
                 
  <div id='GroupName' >
                    
                     <div class="form-group">
                        <label class="col-lg-3 control-label"></label>
                        <div class="col-lg-9">				
                           <span class="col-lg-9 view_rcl" data-toggle="modal" data-target="#myModal2" data-backdrop="false" style='color:#216a94; cursor: pointer;'>View Courses</span>
                        </div>
                     </div>
                  </div>
					   
				   
				 
                 
                  <div class="form-group col-md-12 col-sm-12 col-xs-12">
                     <button type="button" id="save" class="pull-right btn btn-primary">Save</button>
                  </div>
                  {!! Form::close() !!}
                 <!-- <div class="form-group col-md-12 col-sm-12 col-xs-12"><span class="mandatory">Fields marked with an asterisk (*) are mandatory</span>
                  </div>-->
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Modal -->

   <div class="modal fade" id="myModal2" role="dialog">
      <div class="modal-dialog">
         <!-- Modal content-->
         <div class="modal-content" style="width: 300px; margin-left: auto; margin-right: auto; max-height: 400px">
            <div class="modal-header" style="background: linear-gradient(#0275af , #135886 ) !important; padding: 9px 15px 12px 15px;
               color: #fff; font-weight: bold !important; border-bottom: 2px solid #bbbbbb;">
               <button type="button" class="close" data-dismiss="modal" style="color:#fff;opacity: 1;">×</button>
               <h4 class="modal-title">Courses  </h4>
            </div>
            <div class="modal-body" style="overflow-y: scroll;padding:0px; max-height: 400px">
               <table class="table no-margin font14 api_user" style="overflow-y:scroll;">
                  <tbody>
                    @foreach($userdata['courses'] as $valset)
                     <tr>
                         <td style="padding: 9px 15px 12px 15px !important">
                            <h4>{{$valset['title']}} </h4>
                        </td>
                     </tr>
					 @endforeach
                   
                  </tbody>
               </table>
            </div>
            <!--<div class="modal-footer">
               <button type="button" id="click_dash" class="btn btn-sm btn-default btn-flat btn_int_all" data-dismiss="modal">Close</button>
                             
               </div>-->
         </div>
      </div>
   </div>

   <!--modal box-->
</div>
<script src="{{ asset('/js/edit-user.js') }}"></script>
@endsection