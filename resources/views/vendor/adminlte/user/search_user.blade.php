@extends('adminlte::layouts.app')


@section('main-content')
	<div class="wrapper"> 
 <link rel="stylesheet" href="{{ asset('plugins/datatables/dataTables.bootstrap.css')}}">
	 <!-- jQuery 2.2.3 -->
   

    <div class="content-wrapper">
		<div class="container">
			<div class="content">
				
				@if(Session::has('search_user'))
				<div class="alert alert-danger text-center flash-alert" >
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					{{Session::get('search_user')}}
				</div>
				@endif
				@if(Session::has('user_updated'))
				<div class="alert alert-success text-center flash-alert" >
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					{{Session::get('user_updated')}}
				</div>
				@endif
				@if(Session::has('user_notExists'))
				<div class="alert alert-danger text-center flash-alert" >
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					{{Session::get('user_notExists')}}
				</div>
				@endif
	            <div class="col-md-11 basic user-management">
					<div id="tab1_div" class="box box-primary">
						<div class="box-header with-border" style="background: linear-gradient(#0275af , #135886 );">
              <h3 class="box-title" style="color: white;">Manage User</h3>
            </div>

						<div class="box-header with-border" id="filter_user">
						<div class="error" id='err_div' style='display:none'></div> 
						{!! Form::open(['url'=>'user/manage','id' => 'searchUser', 'class' => 'form-horizontal', 'role' => 'form', 'files' => 'true', 'method' =>'POST' ]) !!}
							
			
								
								
								
								
								<div class="form-group">
                                    <label for="name" class="col-lg-3 control-label"> User Name</label>
                                    <div class="col-lg-9">
                                    	{!! Form::text('user_name', null, ['id' => 'user_name', 'class' => 'form-control', 'placeholder' => 'Enter user name']) !!}
                                    </div>
                                </div>
								
								<div class="form-group">
                                    <label for="name" class="col-lg-3 control-label  "> Status</label>
                                    <div class="col-lg-9">
                                        {!! Form::select('status', ['20' => 'All','1' => 'Active', '0' => 'Inactive'], 20, ['class' => 'form-control']) !!}
                                    </div>
                                </div>
								
								<div class="form-group col-md-12 col-sm-12 col-xs-12">
									<button type="submit" id="save" class="pull-right btn btn-primary">Search</button>
								</div>
							{!! Form::close() !!}
						</div>
					</div>
	            </div>
				
					@if(!empty($users))
					@if(!empty($users->items()))
	            <div class="col-md-11 basic">
					<div id="tab1_div" class="manage-client box box-primary">
					<div class="box-header with-border" style="background: rgba(0, 0, 0, 0) linear-gradient(rgb(2, 117, 175), rgb(19, 88, 134)) repeat scroll 0% 0%;">
					<h3 class="box-title" style="color: white;"><i class="fa fa-cogs"></i> Search Results  </h3></div>
						
						<div class="box-header with-border">
							<div class="table no-margin">
								<div class="row heading">
		                    		<div class="col-md-1">User ID</div>
		                    		<div class="col-md-2">User Name</div>
									<div class="col-md-3">Created On</div>
									<div class="col-md-3">Status</div>
									<div class="col-md-3">Action</div>
								</div>
								<div class="row">
			                  		@foreach ($users as $user)
			                    		<div class="col-md-1">{{$user->id}}</div>
										<div class="col-md-2">{{$user->name}}</div>
										<div class="col-md-3"><?php echo $user->created_at; ?></div>
										<div class="col-md-3">{{ $user->status == 1 ? 'Active' : 'Inactive' }}</div>
										<div class="col-md-3"><a href="{{url('user/edit/')}}/{{$user->id}}">Edit</a>  </div>
									@endforeach
			                    </div>
			                </div>
			                {{ $users->render() }}
						</div>
					</div>
	            </div>
	            @endif   @endif

				
	        </div>
	    </div>
		
	</div></div>
	
<script src="{{ asset('plugins/jQuery/jquery-2.2.3.min.js')}}"></script>

	 <!-- SlimScroll -->
<script src="{{ asset('plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
<!-- FastClick -->
<script src="{{ asset('plugins/fastclick/fastclick.js')}}"></script>

	 <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{ asset('plugins/datatables/dataTables.bootstrap.min.js')}}"></script>

@endsection