@extends('adminlte::layouts.app')
@section('main-content')
<?php 
foreach ($data as $key) 
{
      $name = $key->name;
      $email = $key->email;
      $password = $key->password;
      $role = $key->role;
      $created_at = $key->created_at;
}
foreach ($data_chek as $key) 
{
      $dateWeek = $key->recurring;
      $week = $dateWeek;
      $recurring = date('Y.m.d', strtotime($week));
      $current_date = date('Y.m.d');
      $status = $key->payment_status;  
}
?>
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Manage Users
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>
    @if(session()->has('flash_message'))
    <div class="alert alert-success" style="text-align: center;">
        {{ session()->get('flash_message') }}
        </div>
    @endif
      <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border" style="background: linear-gradient(#0275af , #135886 );">
              <h3 class="box-title" style="color: white;">Edit User</h3>
            </div>
            <div style="max-width: 96%; height: 430px; margin-top: 18px;">
                  <form method="POST" action="{{url('/user/update')}}" accept-charset="UTF-8" id="adduser" class="form-horizontal" role="form" enctype="multipart/form-data">  

                    <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                    
                        <div class="form-group">
                                <label for="name" class="col-lg-3 control-label required"> Role</label>
                                <div class="col-lg-9">
                                <select class="form-control" id="role" required="required" name="user_role">
                                      <option value="">Please Select Role</option>
                                      <option value="0"<?php if ($role == '0')  echo "selected"; ?>>Admin</option>
                                      <option value="1"<?php if ($role == '1')  echo "selected"; ?>>User</option>
                                </select>
                                </div>
                        </div>
            
                        <div class="form-group" id="clientName" style="display: none;">
                                <label for="name" class="col-lg-3 control-label required">Select Client</label>
                                <div class="col-lg-9">
                                <input id="name" required="required" class="form-control  " placeholder="Enter client name" name="client_name" type="text" value="ACME.org">

                        </div>
                            </div>
                        <input type="hidden" id="client_id" name="client_id" value="">
                                            
                        <div class="form-group">
                                <label for="name" class="col-lg-3 control-label required">User Name</label>
                                <div class="col-lg-9">
                                    <input required="required" id="user_name" class="form-control" placeholder="Enter user name" name="user_name" type="text" value="<?php echo $name; ?>">
                                </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-lg-3 control-label required">User Email</label>
                            <div class="col-lg-9">
                               
                                <input required="required" id="email" class="api_inputs form-control" placeholder="Enter user email" name="user_email" type="email" value="<?php echo $email; ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="name" class="col-lg-3 control-label required"> User Password</label>
                            <div class="col-lg-9">
                                <input required="required" id="password" class="form-control" placeholder="Enter Password" name="user_password" type="password" value="<?php echo $password; ?>">

                            </div>
                        </div>
               
                        <div class="form-group">
                          <label for="name" class="col-lg-3 control-label required"> Status</label>
                          <div class="col-lg-9">
                              <select class="form-control" name="status">
                              <option value="1" selected="selected">Active</option>
                              <option value="0">Inactive</option></select>
                          </div>
                        </div>

                         <div class="form-group">
                            <label for="name" class="col-lg-3 control-label required">Created on</label>
                            <div class="col-lg-9">
                                <input required="required" id="created_on" class="api_inputs form-control" placeholder="Select Date" name="created_on" type="readonly" value="<?php echo $created_at; ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="name" class="col-lg-3 control-label requied">Payment Status</label>
                            <div class="col-lg-9">
                                <?php 
                                  if($dateWeek === NULL){
                                    ?>
                                    <input required="required" id="single" class="api_inputs form-control" placeholder="Single" name="created_on" type="readonly" value="Single">
                                    <?php
                                    }else{
                                    ?>
                                    <div class="col-lg-6">
                                        <input required="required" id="last_recurense" class="api_inputs form-control" placeholder="Last Recurrence" name="last_recurense" type="readonly" value="<?php echo $current_date; ?>">
                                    </div>
                                    <div class="col-lg-6">
                                      <input required="required" id="next_recurense" class="api_inputs form-control" placeholder="Next Recurrence" name="next_recurense" type="readonly" value="<?php echo $recurring; ?>">
                                    </div>
                                    <?php
                                   }
                                ?>
                            </div>
                        </div>            
                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                      <input type="submit" value="Save" class="pull-right btn btn-primary">
                    </div>
                </form>
             </div>
          </div>
        </div>
      </div>
    </section>
  </div>
</div>
@endsection
