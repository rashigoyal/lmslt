@extends('adminlte::layouts.app')

@section('main-content')
 <div class="content-wrapper">
 <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <!--  <small>Control panel</small> -->
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>
	<section class="content">
	
      <!-- Small boxes (Stat box) -->
      <div class="row">
       <!-- <div class="col-lg-3 col-xs-6">
          <!-- small box 
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>150</h3>

              <p>New Orders</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>-->
        <!-- ./col 
        <div class="col-lg-3 col-xs-6">
          <!-- small box 
          <div class="small-box bg-green">
            <div class="inner">
              <h3>53<sup style="font-size: 20px">%</sup></h3>

              <p>Bounce Rate</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>-->
        <!-- ./col -->
		<?php if($user->role ==0){?>
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>{{$userCount}}</h3>

              <p>Users</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="{{url('user/manage')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>{{$courseCount}}</h3>

              <p>Courses</p>
            </div>
            <div class="icon">
              <i class="fa fa-book" aria-hidden="true"></i>
            </div>
            <a href="{{url('course/manage')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
		<?php }?>
				<?php if($user->role ==1){?>
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>{{$userCourseCount}}</h3>

             <p>Courses</p>
            </div>
            <div class="icon">
              <i class="fa fa-book" aria-hidden="true"></i>
            </div>
            <a href="{{url('course/manage')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
				<?php }?>
        <!-- ./col -->
      </div>
	  <div class='row'>
	  <?php if($user->role ==0){?>
	  <div class="col-md-4">
	<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Recently Added Courses</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <!--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>-->
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body" style="display: block;">
              <ul class="products-list product-list-in-box">
			  @foreach($course as $val)
                <li class="item">
                  <!--<div class="product-img">
                    <img src="dist/img/default-50x50.gif" alt="Product Image">
                  </div>-->
                  <div class="product-info">
                    <a href="javascript:void(0)" class="product-title">
                      <span class="label label-warning pull-right">{{$val->price}}</span></a>
                        <span class="product-description">
						{{$val->title}}
                        </span>
                  </div>
                </li>
				@endforeach
                <!-- /.item -->
                
              </ul>
            </div>
            <!-- /.box-body -->
            <div class="box-footer text-center" style="display: block;">
              <a href="{{url('course/manage')}}" class="uppercase">View All Courses</a>
            </div>
            <!-- /.box-footer -->
          </div></div>
		  <?php }?>
	  </div>
      <!-- /.row -->
      <!-- Main row -->
      
      <!-- /.row (main row) -->

    </section>
</div>
@endsection
