@extends('adminlte::layouts.app')

@section('main-content')
<?php
   
   $access_key         = "AKIAINNUKCYTHKSMUFFQ"; //Access Key
   $secret_key         = "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"; //Secret Key
   $my_bucket          = "digistaklms"; //bucket name
   $region             = "us-east-1"; //bucket region
   //$success_redirect   = 'http://'. $_SERVER['SERVER_NAME'] . '/upload'; //URL to which the client is redirected upon success (currently self) 
	$success_redirect='http://'. $_SERVER['SERVER_NAME'] .'/upload_success';
   $allowd_file_size   = "9048579"; //1 MB allowed Size
   
   //dates
   $short_date       = gmdate('Ymd'); //short date
   $iso_date         = gmdate("Ymd\THis\Z"); //iso format date
   $expiration_date  = gmdate('Y-m-d\TG:i:s\Z', strtotime('+1 hours')); //policy expiration 1 hour from now
   
   //POST Policy required in order to control what is allowed in the request
   //For more info http://docs.aws.amazon.com/AmazonS3/latest/API/sigv4-HTTPPOSTConstructPolicy.html
   $policy = utf8_encode(json_encode(array(
                  'expiration' => $expiration_date,  
                  'conditions' => array(
                     array('acl' => 'public-read'),  
                     array('bucket' => $my_bucket), 
                     array('success_action_redirect' => $success_redirect),
                     array('starts-with', '$key', ''),
                     //array('content-length-range', '1', $allowd_file_size), 
                     array('x-amz-credential' => $access_key.'/'.$short_date.'/'.$region.'/s3/aws4_request'),
                     array('x-amz-algorithm' => 'AWS4-HMAC-SHA256'),
                     array('X-amz-date' => $iso_date)
                     )))); 
   
   //Signature calculation (AWS Signature Version 4)  
   //For more info http://docs.aws.amazon.com/AmazonS3/latest/API/sig-v4-authenticating-requests.html 
   $kDate = hash_hmac('sha256', $short_date, 'AWS4' . $secret_key, true);
   $kRegion = hash_hmac('sha256', $region, $kDate, true);
   $kService = hash_hmac('sha256', "s3", $kRegion, true);
   $kSigning = hash_hmac('sha256', "aws4_request", $kService, true);
   $signature = hash_hmac('sha256', base64_encode($policy), $kSigning);
   ?>
 <div class="content-wrapper">
 <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>
	    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">File Upload</h3>
            </div>
            <!-- /.box-header -->
			 @if(Session::has('no_results'))
      <div class="alert alert-success"><span class="glyphicon"></span><em> {!! session('no_results') !!}</em></div>
      @endif
            <!-- form start -->
           <form action="http://<?= $my_bucket ?>.s3.amazonaws.com/" method="post" enctype="multipart/form-data">
 <input type="hidden" name="key" value="${filename}" />
                                       <input type="hidden" name="acl" value="public-read" />
                                       <input type="hidden" name="X-Amz-Credential" value="<?= $access_key; ?>/<?= $short_date; ?>/<?= $region; ?>/s3/aws4_request" />
                                       <input type="hidden" name="X-Amz-Algorithm" value="AWS4-HMAC-SHA256" />
                                       <input type="hidden" name="X-Amz-Date" value="<?=$iso_date ; ?>" />
                                       <input type="hidden" name="Policy" value="<?=base64_encode($policy); ?>" />
                                       <input type="hidden" name="X-Amz-Signature" value="<?=$signature ?>" />
                                       <input type="hidden" name="success_action_redirect" value="<?= $success_redirect ?>" /> 
              <div class="box-body">
                 <!--<div class="form-group">
                  <label for="exampleInputEmail1">Email address</label>
                  <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Password</label>
                  <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                </div> -->
                <!--<div class="form-group">
                  <label>Upload</label>
                  <input type="file" id="exampleInputFile">
<span class="help-block">Help block with error</span>
                  <!--<p class="help-block">Example block-level help text here.</p>
                </div>-->
				                                      <div class="form-group" id='clientName' style='display:block'>
                                          <label for="name" class="col-lg-3 control-label required">Upload</label>
                                          <div class="col-lg-9">
                                              <div id="upload-containe">
<!--                                                   <button type="button" id="upload-browse-button" class="btn btn-primary">Browse...</button> -->
                                                  <input type="file"  style="color: #000;padding: 6px 30px;font-size: 15px;" name="file" />
                                                  <p id="currentUploadedFile"></p>
                                                  <div class="progress progress-striped active hidden" id="upload-progress">
                                                      <div class="progress-bar"  role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
                                                      </div>
                                                  </div>
                                              </div>
                                              <div id="error-upload" style="color: red"></div>
                                          </div>
                                      </div>
                <!--<div class="checkbox">
                  <label>
                    <input type="checkbox"> Check me out
                  </label>
                </div>-->
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
		</div>
		</div>
	</section>
</div>
@endsection
