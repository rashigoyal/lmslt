/*
SQLyog Community v12.3.3 (64 bit)
MySQL - 10.1.22-MariaDB : Database - lms
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`lms` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `lms`;

/*Table structure for table `checkout_lms` */

DROP TABLE IF EXISTS `checkout_lms`;

CREATE TABLE `checkout_lms` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL,
  `payment_status` enum('0','1') DEFAULT NULL COMMENT '0-Cancel 1-Success',
  `order_number` int(100) DEFAULT NULL,
  `card_holder_name` varchar(100) DEFAULT NULL,
  `recurring` varchar(255) DEFAULT NULL,
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `checkout_lms` */

insert  into `checkout_lms`(`id`,`user_id`,`payment_status`,`order_number`,`card_holder_name`,`recurring`) values 
(1,NULL,'',78458748,'amit',NULL);

/*Table structure for table `courses` */

DROP TABLE IF EXISTS `courses`;

CREATE TABLE `courses` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` text,
  `description` text,
  `price` varchar(255) DEFAULT NULL,
  `schedule` varchar(255) DEFAULT NULL,
  `duration` varchar(255) DEFAULT NULL,
  `summary` text,
  `skills` text,
  `mode` enum('0','1') DEFAULT NULL COMMENT '0-Online 1 -Instructor',
  `random_token` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `courses` */

insert  into `courses`(`id`,`title`,`description`,`price`,`schedule`,`duration`,`summary`,`skills`,`mode`,`random_token`) values 
(1,'Contact','Desp1','100','mon-fri','10weeks','COnt1','PHP','1','fdc136f6301b6a08f26332ce928a9eaa'),
(2,'Contact','Desp1','100','mon-fri','10weeks','COnt1','PHP','1','5f90ebd5c2c5d73f75e6c571a6658933'),
(3,'Contact','Desp1','100','mon-fri','10weeks','Summ1','PHP','1','a14ba6f6a663fd1d38cd1c4d94b4baea'),
(4,'Contact','Desp1','100','mon-fri','10weeks','Summ1','PHP','1','69f29a344ccadcd1c7976d046ca2d8c7'),
(5,'Contact','Desp1','100','mon-fri','10weeks','Summ1','PHP','1','5757baeee58a1fea8549b99f9920cf99'),
(6,'Contact','Desp1','100','mon-fri','10weeks','Sum1','PHP','1','a8ee99c8b9b81810973e363d153009db'),
(7,'Contact','Desp1','100','mon-fri','10weeks','Sum1','PHP','1','da7cacb696a66a59ecef999fc88a3742');

/*Table structure for table `media` */

DROP TABLE IF EXISTS `media`;

CREATE TABLE `media` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `path` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `ext` varchar(255) DEFAULT NULL,
  `gallery_id` varchar(255) DEFAULT NULL,
  `status` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `media` */

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `migrations` */

/*Table structure for table `s3_bucket` */

DROP TABLE IF EXISTS `s3_bucket`;

CREATE TABLE `s3_bucket` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `key` text,
  `bucket` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `s3_bucket` */

insert  into `s3_bucket`(`id`,`key`,`bucket`) values 
(1,'2015_12_15_22.33.19.png','digistaklms'),
(2,'2015_12_15_22.33.19.png','digistaklms'),
(3,'2015_12_15_22.33.19.png','digistaklms'),
(4,'2015_12_18_11.30.54.png','digistaklms'),
(5,'2015_12_18_11.30.54.png','digistaklms'),
(6,'2015_12_15_22.33.19.png','digistaklms'),
(7,'2015_12_15_19.46.47.png','digistaklms'),
(8,'1494394017.3gp','digistaklms');

/*Table structure for table `section` */

DROP TABLE IF EXISTS `section`;

CREATE TABLE `section` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `section_id` int(20) DEFAULT NULL,
  `section_name` varchar(255) DEFAULT NULL,
  `course_id` int(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `section` */

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`name`,`email`,`password`,`address`,`remember_token`,`created_at`,`updated_at`) values 
(1,'admin','admin@syptus.com','$2y$10$55yGUJLF20ht314YB7SnFuACLgcCkhYTg3i0zGZXhiwKz4lkC6E2i',NULL,'bExDcfdpuCtzmxIOh5aBosYmoypVutBXQUkrDmh4sXCYhY0v7od4X2kNsgo5','2017-05-15 08:00:09','2017-05-15 08:00:09');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
