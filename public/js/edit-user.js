

jQuery(document).ready(function($){

$(document).on("click", "#save", function() {
        var alertSet = $("#err_div").is(":visible");
       var emailerror= nameerror = 0;
        var action = $('#action').val();
        var val = $('#name').val();
       
		
		//$("#save").html('<i class="fa fa-refresh fa-lg fa-spin" style="color: #ffffff;"></i>');
		

		if ($('#user_name').val() == '') {
				$('#err_div').html("Please enter user name.");
				if (alertSet == false) $('#err_div').toggle();
				return false;
		}
		if ($('#email').val() == '') {
			$('#err_div').html("Please enter email.");
			if (alertSet == false) $('#err_div').toggle();
			return false;
		}
		if (/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/.test($('#email').val()) == false) {
			$('#err_div').html("Please enter valid email.");
			if (alertSet == false) $('#err_div').toggle();
			return false;
		}
		
		if ($('#password').val() == '') {
			$('#err_div').html("Please enter password.");
			if (alertSet == false) $('#err_div').toggle();
			return false;
		}
			
		if (emailerror == 1) {
			$('#err_div')
				.html("The email address provided already exists. Please try another email address.");
			if (alertSet == false) $('#err_div')
				.toggle();
			$("#save").html('Save');
			return false;
		}
		
		
			if (emailerror == 0 ) {
				if (alertSet == true) $('#err_div').toggle();
				$('#err_div').hide();
				$("#save").html('Save');
				$('#adduser').submit();
			}
		
		
			if (emailerror == 0  && nameerror == 0) {
				if (alertSet == true) $('#err_div').toggle();
				$('#err_div').hide();
				$("#save").html('Save');
				$('#adduser').submit();
			}
		
		
	});


});

