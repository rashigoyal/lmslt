<?php

/*
 * Taken from
 * https://github.com/laravel/framework/blob/5.3/src/Illuminate/Auth/Console/stubs/make/controllers/HomeController.stub
 */

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use Illuminate\Support\Facades\Crypt;
use Session;
use App\S3bucket;
use App\Course;
use App\Section;
use App\User;
use App\UserCourse;


/**
 * Class CourseController
 * @package App\Http\Controllers
 */
class CourseController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index()
    {
		return view('adminlte::home')->with([
            'user' => Auth::user(),
            'menu' => [
                'tree' => '',
                'item' => ''
            ]
        ]);
    }
	
   public function new_course() {
    Session::forget('no_results');  

     
        $data = [
            'pagetitle' => 'New  Course',
            'page_title' => 'New Course '
           
        ];
         


         return view('adminlte::course.new_course')->with([
            'data' => $data,
            'menu' => [
                'tree' => '',
                'item' => 'add-course'
            ]
        ]);  
        
    }	

    public function new_post_course( Request $request ) {

          $save = array();
          $input = Input::all();
          $title =  Input::get('title');
          $price =  Input::get('price');
          $duration_value =  Input::get('duration_value');
          $duration_weeks =  Input::get('duration_weeks');
          $schedule =  Input::get('schedule');
          $description =  Input::get('description');
          $summary =  Input::get('summary');
          $skills =  Input::get('skills');
          $mode =  Input::get('mode');
          $duration = $duration_value . $duration_weeks;
            $data = [
            'pagetitle' => 'New  Course',
            'page_title' => 'New Course '
           
        ];
         
         $random_token = uniqid(rand(10,10000000),true);
          $random_token  = md5($random_token);
          $save = array(

            'title' => $title,
            'price' => $price,
            'duration' => $duration,
            'schedule' => $schedule,
            'description' => $description,
            'summary' => $summary,
            'skills' => $skills,
            'mode' => $mode,
            'random_token' => $random_token


            ); 

           $s3 = S3bucket::select('*')->get();
            Course::insert($save);
            $course = Course::select('id')->where('random_token', '=', $random_token)->first();
               return view('adminlte::course.new_section')->with([
            'data' => $data,
            'course' => $course,
            's3' => $s3,
            'menu' => [
                'tree' => '',
                'item' => 'add-course'
            ]
        ]);  



    }



    // public function new_post_section( Request $request ) {

		
    //              $s3 = S3bucket::select('*')->get();
    //     return view('adminlte::course.new_section')->with([
    //        's3' => $s3,
           
    //         'menu' => [
    //             'tree' => '',
    //             'item' => 'file-upload'
    //         ]
    //     ]);  





    // }


    public function new_section_add()
    {

       $course_id = $_GET['course_id'];
       $counter = $_GET['counter'];
        $random_token = uniqid(rand(10,10000000),true);
          $random_token  = md5($random_token);
          if ($counter == 1) { 
           $save = array(


              'section_id' => $counter,
              'course_id' => $course_id,
              'random_token' => $random_token



            );

          Section::insert($save);


            $section = Section::select('*')->where('random_token', '=', $random_token)->first();


             return response()->json($section); 





         }

         else
         {

            $title = $_GET['title'];
            $count = $counter - 1;
            $random_token = uniqid(rand(10,10000000),true);
            $random_token  = md5($random_token);
            $save_new = array(

                  'random_token' => $random_token,
                  'section_name' => $title

              );
             Section::where('section_id',$count)
                    ->where('course_id', $course_id)
                    ->update($save_new);
           

            $section_title = Section::select('*')->where('random_token', '=', $random_token)->first();
            $token = uniqid(rand(10,10000000),true);
            $token  = md5($token);
            $save = array(


              'section_id' => $counter,
              'course_id' => $course_id,
              'random_token' => $token



            );
            Section::insert($save);
            $section_append = Section::select('*')->where('random_token', '=', $token)->first();
            //print($section_title);
            $data = array();
            $data['section_title'] = $section_title;
            $data['section_append'] = $section_append;
                 return response()->json($data); 

         }

    }

    public function new_post_preview( Request $request )
    {

         $input = Input::all();
         $counter = $input['counter'];
         $cpo = $counter - 1;
         $section = $request['section_title'.$cpo];
         $course_id = $request['course_id'];
         // last section title 
         $random_token = uniqid(rand(10,10000000),true); 
         $random_token  = md5($random_token);         
          $save_new = array(

                'random_token' => $random_token,
                'section_name' => $section

            );
            Section::where('section_id',$counter)
                    ->where('course_id', $course_id)
                    ->update($save_new);


         //Add Videos

        $set = array();
        for($i=1;$i<=$counter;$i++){
            
            $set[$i] = $request['account_id_'.$i];

        }
        foreach($set as $key => $value) {
             $save_n = array(

               'video_id' => $value


              );
              Section::where('section_id',$key)
              ->where('course_id', $course_id)
               ->update($save_n);


        }

          Session::flash('no_course', 'Course Created.');

         return view('adminlte::course.manage_course')->with([
            'user' => Auth::user(),
            'menu' => [
                'tree' => '',
                'item' => 'manage-course'
            ]
        ]);


     

  }


  public function manage_course()
  {
  //$course = Course::select()->get();
  $course = Course::with('sections')->get();
  foreach ($course as $key) 
  {
    $id = $key->id;
    $section[] = Section::where('course_id', $id)->count();
  }
  /*$course = Course::with('sections')->get();
  $section =  $course[0]['sections'][0];
  echo "<pre>";
  print_r($section['id']); die;*/
  return view('adminlte::course.manage_course')->with([
            'user' => Auth::user(),
            'course' => $course,
            'section' => $section,
            'menu' => [
                'tree' => '',
                'item' => 'manage-course'
            ]
        ]);
  }

  
  public function view_course($id){
	  
	  if(Auth::user()->role == 1){
		  $chk = UserCourse::where('course_id' , $id)->where('user_id',Auth::user()->id)->get();
		  if(count($chk) == 0){
			  return redirect('/home');
		  }
	  }
	  $course = Course::with('users','sections')->find($id);
	  if(count($course) == 0){
			  return redirect('/home');
		  }
		$video =  $vid = array();
	  if(!empty($course['sections'])){
		  foreach($course['sections'] as $val){
			  $vid[] = $val->video_id;
		  }
	  }
	  $video = S3bucket::whereIn('id',$vid)->get();
	 //dd($course);
	   return view('adminlte::course.view_course')->with([
            'user' => Auth::user(),
			'course'=>$course,
			'video'=>$video,
            'menu' => [
                'tree' => '',
                'item' => 'view-course'
            ]
        ]);
	  
  } 

    public function searchCourse(Request $request)
    {   $course  = array();
        
        $data = [
            'pagetitle' => 'Manage Course'.' - LMS',
            'page_title' => 'Manage Course'
        ];
        //$userrole = Auth::user()->role;
        if ($request->get('page')) {
           
        $course_name = Input::get ('course_name');
	
		$course = '';
        
                $course = Course::with('users','sections');

	    if ($course_name != '')
		{
			$course->where('title', $course_name);
		}
		
		$course = $course->paginate('2'); 
			
        $total_count = count($course); 
			
			
            $data['course'] = $course;
            
                  $course = $course->appends ( array (
                    'status' => Input::get ('status'),
                ) );
            
            
        } 
        return view('adminlte::course.manage_course')->with([
           'course' => $course,
            'menu' => [
                'tree' => '',
                'item' => 'course-manage'
            ]
        ]);  
    }

    public function postSearchCourse(Request $request)
    {   $course =array();
		$course_name = Input::get ('course_name');
		
		$course = '';
        
        $course = Course::with('users','sections');

	    if ($course_name != '')
		{
			$course->where('title', $course_name);
		}
		
		$course = $course->paginate('2'); 
        $total_count = count($course); 
        //echo "<pre>";print_r($users[0]['clients']['']);echo "</pre>";
		//$cou = Course::with('users','sections')->find(1);
       // dd($cou);
        //echo $total_count; die;
       
        if($total_count == 0){
            Session::flash('search_user', 'No record found');
            return view('adminlte::course.manage_course', $data);
        }
        else{ 
                    $course = $course->appends ( array (
                ) );
             
			 return view('adminlte::course.manage_course')->with([
             'course' => $course,
            'page_title' => 'Manage Users',
            'menu' => [
                'tree' => '',
                'item' => 'course-manage'
            ]
        ]);  
        }
    }



}