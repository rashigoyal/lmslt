<?php

/*
 * Taken from
 * https://github.com/laravel/framework/blob/5.3/src/Illuminate/Auth/Console/stubs/make/controllers/HomeController.stub
 */

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use Illuminate\Support\Facades\Crypt;
use Session;
use App\S3bucket;
use App\Course;
use App\Section;
use App\User;
use App\Checkoutlms;
use App\UserCourse;
use Mail;

/**
 * Class ActivateController
 * @package App\Http\Controllers
 */
class ActivateController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth'], ['except' => ['activate_user','password_link','save_user','user_new']]);
    }
	
	  public function activate_user($token){
		  
		  $data = json_decode(base64_decode($token));
		  $email = $data->email;
		  $name = $data->first_name;
		  $salt_1 = uniqid(rand(10,1000),true);
		  $salt_1 = $salt_1 . $name;
		  $shuffled_id = md5($salt_1);
		  $shuffled_id = substr($shuffled_id,-8);
		  $shuffled = bcrypt($shuffled_id);
			/*	if(User::where('email','=', $email)->exists()){
                  
                }
				else ( */
				$role = '1';
                $salt = uniqid(rand(10,10000000),true);
                $salt = $salt . $email;
                $saltid  = md5($salt);
				$data_id = $saltid;		
				$date = date('Y-m-d H:i:s');
				$saveUsers = array(
					'email' => $email,
					'random_token' => $data_id,
					'name' => $name,
					'role' => $role,
					'password' => $shuffled,
					'Created_at' => $date

				); 

							

				User::insert($saveUsers);
				$id = User::where('random_token',$data_id)->first();
				$user_id = $id->id;
				$email = $id->email;
				$url = array();
				$url['email'] = $email;
				$url['user_id'] = $user_id;
				$url['name'] = $name;
				$url['random_token'] = $data_id;
				$url['shuffled_id_pass'] = $shuffled;
				$url['shuffled_id'] = $shuffled_id;
				$url['flag'] = '1';
				//echo "<pre>"; print_r($url); echo "</pre>";echo "<br>";
				$base = base64_encode(json_encode($url));
				//echo "<pre>"; print_r($base); echo "</pre>";echo "<br>";
				 $data_url = url('/');
				  $use = user::select('name')->where('email', '=', $email)->first();
                  $data = "Hi  " . $use->name . "," . PHP_EOL . PHP_EOL .
				  "Your account has been created successfully. You can login by clicking here " . $data_url . "/login. Login details are given below:" . PHP_EOL .
				  "Email:" .$email. PHP_EOL .
				  "Password:" .$shuffled_id. PHP_EOL . PHP_EOL .
				  "Thanks" ;
                  
               
                Mail::raw( $data , function($message){
                
                $message->to('goyalrockin@gmail.com')->subject('Account Information');
                
                });
				
				
				
				/* $data_url = url('/');
				$data_email = "Hi," . PHP_EOL . "Your password reset link is " . $data_url . "/password/reset/" . $data_id . PHP_EOL . "Please open this link to reset your password." . PHP_EOL . "Thanks" ;
				$email = $data->email;
				$data_el = array( 'email' => $email);

				Mail::raw( $data_email , function($message)  use ($data_el){
                
					$message->to($data_el['email'])->subject('Reset Password');
                
                }); */
				return redirect('/login?'.$base);	



				//die;

				
				//Checkoutlms::insert($savecheckout);
			   /*$product_id = $data->li_0_product_id;
				$save_course = array(

					'user_id' => $id,
					'course_id' => $product_id

				  );*/
	
				//print_r($email);
				//Session::flash('user_updated', 'User Created Successfully');
				// return view('adminlte::password.newuser')->with([
				// 	'email' => $email,
				// 	'data_id' => $data_id,
				// 	'user_id' => $user_id,
				// 	'page_title' => 'New User',
				// 	'id' => $id,
				// 	'menu' => [
				// 		'tree' => '',
				// 		'item' => 'new_user'
				// 	]
				// ]); 
			//	}
		    
		  
		  
	  }
	  public function save_user()
	  {
	  		$input = Input::all();
	  		$name = $input['user_con'];
	  		$password = bcrypt($input['pass_con']);
	  		$email = $input['email'];
	  		$random_token = $input['token_salt'];
	  		$user_id = $input['user_id'];
	  		$role = '1';
	  		$update = array(

	  				'name' => $name,
	  				'password' => $password,
	  				'role' => $role

	  			);

			$course = Checkoutlms::select('json')->where('user_id',$user_id)->first();
	  			$course =  $course ->json;
                $base = base64_decode($course);
                $json = json_decode($base); 
                $counter = $json->counter;
                $co_id = array();
                for ($i=0; $i < $counter ; $i++) { 
                	$j = 'li_'.$i.'_name';
                	$course = $json->$j;
                	// $id = Course::select('id')->where('title',$course)->first();
                	// $co_id[] = $id->id;
             		$res = Course::where('title', $course)->exists();
                       if($res)
                       {
                           $id = Course::select('id')->where('title',$course)->first(); 
                       }
                       else
                       {
                           $save = array(
                            'title' => $course, 
                           );
                           Course::insert($save);
                           $id = Course::select('id')->where('title', $course)->first();
                       }
                       $co_id[] = $id->id; 
                	
                	
                }
                $size  = sizeof($co_id);
                for ($i=0; $i < $size; $i++) { 
                	
                		$save_course = array(

                				'course_id' => $co_id[$i],
                				'user_id' => $user_id

                			);

                	UserCourse::insert($save_course);

                }






          User::where('random_token',$random_token)
            ->update($update);
            return redirect('/');

           
			
	  }
	
	
}