<?php

/*
 * Taken from
 * https://github.com/laravel/framework/blob/5.3/src/Illuminate/Auth/Console/stubs/make/controllers/HomeController.stub
 */

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use Illuminate\Http\Request;
use Session;
use Auth;
use App\S3bucket;
use App\User;
use App\UserCourse;
use App\Course;
use App\Checkoutlms;
use DB;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    
    
	
	 // User Search
    public function searchUser(Request $request)
    {   $user  = array();
        $userId = '';
        $data = [
            'pagetitle' => 'Manage Users'.' - LMS',
            'page_title' => 'Manage Users'
        ];
        //$userrole = Auth::user()->role;
        if ($request->get('page')) {
           
            
          $user_name = Input::get ('user_name');
		$userId = $request->get('user_id');
		  $status = Input::get ('status');
           
            
            //echo 'aayyya'.$clientId;die;
			$users = '';
        
        $user = User::select('name','id','created_at','email');

		if ($user_name != '')
		{
		$user->where('name', $user_name);
		}
		if($status != 20){
		$user->where('statuss', $status);
		}

		$user = $user->with('courses')->paginate('2');
			
        $total_count = count($user); 
			
			
            $data['users'] = $user;
            
                  $user = $user->appends ( array (
                    'status' => Input::get ('status'),
                ) );
            
            
        } 
        return view('adminlte::user.search_user')->with([
           'users' => $user,
            'menu' => [
                'tree' => '',
                'item' => 'user-manage'
            ]
        ]);  
    }

    public function postSearchUser(Request $request)
    {
		$user_name = Input::get ('user_name');
		$userId = $request->get('user_id');
		$status = Input::get ('status');


		$users = '';
        
        $user = User::select('name','id','created_at','email');

		if ($user_name != '')
		{
		$user->where('name', $user_name);
		}
		if($status != 20){
		$user->where('statuss', $status);
		}

		$user = $user->with('courses')->paginate('2');
        $total_count = count($user); 
        //echo "<pre>";print_r($users[0]['clients']['']);echo "</pre>";
        
        //echo $total_count; die;
       
        if($total_count == 0){
            Session::flash('search_user', 'No record found');
            return view('user.search_user', $data);
        }
        else{ 
           
                    $user = $user->appends ( array (
                    
                    'status' => Input::get('status')
                ) );
             
            
           
			 return view('adminlte::user.search_user')->with([
             'users' => $user,
            'page_title' => 'Manage Users',
            'menu' => [
                'tree' => '',
                'item' => 'user-manage'
            ]
        ]);  
        }
    }
	
	public function getEditUser($id)
    {
        $userdata = User::with('courses')->find($id);
		if(count($userdata)>0){
		
			 return view('adminlte::user.edit_user')->with([
            'page_title' => 'Edit User',
			'action' => 'edit',
			'userdata' => $userdata,
            'menu' => [
                'tree' => '',
                'item' => 'edit-user'
            ]
        ]);  
		}
		else{
			Session::flash('user_notExists', 'User does not exist in the database.');
			return redirect('user/manage');
		}
    }
	
	public function postEditUser( Request $request)
    {
        
        $all = $request->all();
      
        $param =array();
		$userdet      = Auth::user();
        $id= $all['user_id'];
        $param['name'] =  $all['user_name'];
       
        $param['email'] =  $all['user_email'];
		
		$user_detail = User::where('id',$id)->first();
        if($all['user_password'] != ''){
			if($all['user_password'] != $user_detail['password'])
			{
				$param['password'] = bcrypt($all['user_password']);
			}
        }
		//echo $param['password']; die;
        $param['status'] =  $all['status'];
       
		
		$updated_at = date('Y-m-d H:i:s');
		$param['updated_at'] = $updated_at;
        
	
           $user =  User::where('id',$id)->update($param);
        

        
        if ($user) {
            Session::flash('user_updated', 'User updated successfully');
        }
		
		
        return redirect('user/manage');
    }
	
	public function user_courses()
    {
        $user = Auth::user();
		$userCourseCount  = 0;
		$userCourse  = User::with('courses')->find($user->id);
		$userCourseCount  =  count($userCourseCount['courses']);
		
		 return view('adminlte::user.my_courses')->with([
		'page_title' => 'Edit User',
		'action' => 'edit',
		'userCourse' => $userCourse,
		'menu' => [
			'tree' => '',
			'item' => 'edit-user'
		]
		]);  
		
    }

}