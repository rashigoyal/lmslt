<?php

/*
 * Taken from
 * https://github.com/laravel/framework/blob/5.3/src/Illuminate/Auth/Console/stubs/make/controllers/HomeController.stub
 */

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Crypt;
use Session;
use App\S3bucket;
use App\User;
use App\Course;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
	 
	 public function login()
    {
        
		return view('adminlte::auth.login');
	}
    public function index()
    {
		$userCount = User::get();
		$userCount = count($userCount);
		$course = Course::orderBy('created_at','DESC')->limit(5)->get();
		$courseCount = count($course);
		$user = Auth::user();
		$userCourseCount  = 0;
		$userCourseCount  = User::with('courses')->find($user->id);
		$userCourseCount  =  count($userCourseCount['courses']);
		
		return view('adminlte::home')->with([
            'user' => $user,
			'userCount'=>$userCount,
			'courseCount'=>$courseCount,
			'userCourseCount'=>$userCourseCount,
			'course' =>$course,
            'menu' => [
                'tree' => '',
                'item' => ''
            ]
        ]);
    }
	
	
	public function s3upload()
    {	Session::forget('no_results');	
		return view('adminlte::s3upload')->with([
            'user' => Auth::user(),
            'menu' => [
                'tree' => '',
                'item' => 'file-upload'
            ]
        ]);
    }
	
	 public function s3uploadsucess() {

            $my_bucket = "digistaklms";
            $key = $_GET['key'];
            $save = array(

                    'bucket' => $my_bucket,
                    'key' => $key,

                );

           S3bucket::insert($save);

           Session::flash('no_results', 'File Uploaded.');

            return view('adminlte::s3upload')->with([
            'user' => Auth::user(),
            'menu' => [
                'tree' => '',
                'item' => 'file-upload'
            ]
        ]);


         
         
         }
}