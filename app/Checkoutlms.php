<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Checkoutlms extends Model
{    protected $table = 'checkout_lms';
     protected $fillable = [
        'i', 'key','etag','bucket'];

}