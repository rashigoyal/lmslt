<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserCourse extends Model
{    protected $table = 'user_course';
     protected $fillable = [
        'id', 'user_id','course_id'
	];
	/* public function courses(){
        return $this->belongsTo('App\Course','id','course_id');
    }
	
	public function users()
    {
        return $this->hasMany('App\User','id','user_id');
    } */
}