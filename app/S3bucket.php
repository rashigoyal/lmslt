<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class S3bucket extends Model
{    protected $table = 's3_bucket';
     protected $fillable = [
        'id', 'key','etag','bucket'];

}