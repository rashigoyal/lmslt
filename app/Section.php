<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{    protected $table = 'section';
     protected $fillable = [
        'id','section_id', 'section_name','course_id'];
	
	public function videos()
    {
        return $this->hasMany('App\S3bucket');
    }
}