<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{    protected $table = 'courses';
     protected $fillable = [
        'id', 'title','description','price','schedule','duration','summary','skills','mode'];
		
		
	public function users()
    {
        return $this->belongsToMany('App\User', 'user_course', 
      'course_id', 'user_id');
    }
	
	public function sections()
    {
        return $this->hasMany('App\Section');
    }
}